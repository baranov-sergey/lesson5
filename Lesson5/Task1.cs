﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project1
{
    class Task1
    {
        //Заменить в строке все вхождения 'test' на 'testing'. Удалить из текста все символы, являющиеся цифрами.

        public static void NoDigitAndTestReplaceTesting()
        {
            Console.WriteLine("Enter text to check");
            string str = Console.ReadLine();
            StringBuilder sb = new StringBuilder();
            foreach (var c in str)
            {
                if (!char.IsDigit(c))
                {
                    sb.Append(c);
                }
            }
            sb.Replace("test", "testing");
            Console.WriteLine("Got a string:\n" + sb);
        }
    }
}

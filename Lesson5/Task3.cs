﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson5
{
    internal class Task3
    {
        //3. Дана строка: teamwithsomeofexcersicesabcwanttomakeitbetter.
        //Необходимо найти в данной строке "abc", записав всё что до этих символов в первую переменную,
        //а также всё, что после них во вторую.
        //Результат вывести в консоль.

        public static void StringSplit()
        {
            Console.WriteLine("Given the line: teamwithsomeofexcersicesabcwanttomakeitbetter");
            string str = "teamwithsomeofexcersicesabcwanttomakeitbetter";
            string hf1 = string.Empty, hf2 = string.Empty;

            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] == 'a' && str[i + 1] == 'b' && str[i + 2] == 'c' && i + 2 < str.Length)
                {
                    hf1 = str.Substring(0, i + 3);
                    hf2 = str.Substring(i + 3, str.Length - (i + 3));
                }
            }
            Console.WriteLine($"Left half: {hf1}\nRight half: {hf2}");
        }
    }
}


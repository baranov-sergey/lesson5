﻿using Lesson5;
using Microsoft.VisualBasic;
using Project1;

internal class Program
{
    private static void Main(string[] args)
    {
        while (true)
        {
            Console.WriteLine(
            "\nWhat task will we choose(number)?\n" +
            "[1] NoDigitAndTestReplaceTesting\n" +
            "[2] StringConcatenation\n" +
            "[3] StringSplit\n" +
            "[4] GoodDay\n" +
            "[5] OutputBlocksOfNumbers\n" +
            "[6] OutputBlocksOfLetters\n" +
            "[7] CheckABC\n" +
            "[8] CheckStartApp555\n" +
            "[9] CheckEndApp1a2b\n");

            if (!int.TryParse(Console.ReadLine(), out int numberTask))
            { Console.WriteLine("Incorrect value, please re-enter the operation"); continue; }
            Console.WriteLine();

            string str = string.Empty;
            if (numberTask >= 5)
            {
                Console.WriteLine("Enter the document number in the format xxxx-yyy-xxxx-yyy-xyxy");
                str = Console.ReadLine();
            }

            switch (numberTask)
            {
                case 1: Task1.NoDigitAndTestReplaceTesting(); break;
                case 2: Task2.StringConcatenation(); break;
                case 3: Task3.StringSplit(); break;
                case 4: Task4.GoodDay(); break;
                case 5: Task5.OutputBlocksOfNumbers(str); break;
                case 6: Task5.OutputBlocksOfLetters(str); break;
                case 7: Task5.CheckABC(str); break;
                case 8: Task5.CheckStartApp555(str); break;
                case 9: Task5.CheckEndApp1a2b(str); break;
                default: Console.WriteLine("Incorrect task number, please re-enter the number"); continue;
            }
        }
    }
}
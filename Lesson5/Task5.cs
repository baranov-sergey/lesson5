using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static System.Reflection.Metadata.BlobBuilder;

namespace Lesson5
{
    internal class Task5
    {
        //5. Написать программу со следующим функционалом:
        //На вход передать строку(будем считать, что это номер документа).
        //Номер документа имеет формат xxxx-yyy-xxxx-yyy-xyxy, где x — это число,
        //а y — это буква.
        //Все эти методы реализовать в отдельном классе в статических методах,
        //которые на вход(входным параметром) будут принимать вводимую на
        //вход программы строку.

        public static void OutputBlocksOfNumbers(string str)
        {
            //- Вывести на экран в одну строку два первых блока по 4 цифры.

            Console.WriteLine();
            Regex regex = new Regex(@"\d{4}");
            MatchCollection matches = regex.Matches(str);
            if (matches.Count > 0)
            {
                Console.Write("Output string: ");
                foreach (Match match in matches)
                {
                    Console.Write(match.Value + " ");
                }
                Console.WriteLine();
            }
            else
            {
                Console.WriteLine("Nothing found\n");
            }
        }
        public static void OutputBlocksOfLetters(string str)
        {
            //- Вывести на экран номер документа, но блоки из трех букв заменить
            //на*** (каждая буква заменятся на*).

            Console.WriteLine();
            Regex regex = new Regex(@"[a-zA-Z]{3}");
            string target = "***";
            string result = regex.Replace(str, target);
            Console.WriteLine("Output string: " + result);
        }
        public static void CheckABC(string str)
        {
            //- Проверить содержит ли номер документа последовательность abc и
            //вывети сообщение содержит или нет(причем, abc и ABC считается
            //одинаковой последовательностью).

            Console.WriteLine();
            str.ToLower();
            Regex regex = new Regex(@"abc");
            MatchCollection matches = regex.Matches(str);
            if (matches.Count > 0)
            {
                Console.WriteLine($"Document number contains abc {matches.Count} time");
            }
            else
            {
                Console.WriteLine("Nothing found");
            }
        }
        public static void CheckStartApp555(string str)
        {
            //- Проверить начинается ли номер документа с последовательности
            //555.

            Console.WriteLine();
            Regex regex = new Regex(@"^555");
            bool result = regex.IsMatch(str);
            if (result)
            {
                Console.WriteLine("Document number starts with 555");
            }
            else
            {
                Console.WriteLine("Document number does not contain 555 at the beginning");
            }
        }
        public static void CheckEndApp1a2b(string str)
        {
            //- Проверить заканчивается ли номер документа на
            //последовательность 1a2b.

            Console.WriteLine();
            Regex regex = new Regex(@"1a2b$");
            bool result = regex.IsMatch(str);
            if (result)
            {
                Console.WriteLine("Document number ends with 1a2b");
            }
            else
            {
                Console.WriteLine("Document number does not end with 1a2b");
            }
        }
    }
}

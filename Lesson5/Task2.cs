﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project1
{
    public class Task2
    {
        //2.Используя метод вывода значения в консоль,
        //выполните конкатенацию слов и выведите на экран следующую фразу:
        //Welcome to the TMS lesons.
        //Каждое слово должно быть записано отдельно и взято в кавычки, например "Welcome".
        //Не забывайте о пробелах после каждого слова

        public static void StringConcatenation()
        {
            Console.WriteLine("Welcome " + "to " + "the "+ "TMS "+ "lesons.");
        }
    }
}

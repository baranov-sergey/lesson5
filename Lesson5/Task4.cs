﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson5
{
    //4. Дана строка: Плохой день.
    //Необходимо с помощью метода substring удалить слово "плохой".
    //После чего необходимо используя команду insert создать строку со значением: Хороший день!!!!!!!!!.
    //Заменить последний "!" на "?"

    internal class Task4
    {
        public static void GoodDay()
        {
            string str = "Плохой день.";
            Console.WriteLine("Given the line: " + str);
            str = str.Substring(6, str.Length - 6);
            str = str.Insert(0, "Хороший");
            str = str.Insert(12, "!!!!!!!!!");
            Console.WriteLine("Got a string: " + str);
        }
    }
}
